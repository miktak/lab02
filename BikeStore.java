//2036737
//Mikito Takata
public class BikeStore {
 
    public static void main(String args[])
    {
        Bicycle[] bikes = new Bicycle[4];

        bikes[0] = new Bicycle("Bikees", 10, 50.5);
        bikes[1] = new Bicycle("Almost a Car", 55, 90.6);
        bikes[2] = new Bicycle("slow bikes", 5, 20.0);
        bikes[3] = new Bicycle("You're really better off walking", 1, 0.5);

        for(int i=0; i < bikes.length; i++)
        {
            System.out.println(bikes[i]);
        }
    }

}
